# [346. Moving Average from Data Stream] (https://leetcode.com/problems/moving-average-from-data-stream/)

Algorithm is trivial. Only thing to take care of is using the right data structure so that the remove opertation is constant time. 

```
public class MovingAverage {

    List<Integer> m_window;

    int m_curr_size;

    int m_size;

    int m_sum;

    /** Initialize your data structure here. */
    public MovingAverage(int size) {
        m_window = new LinkedList<Integer> ();
        m_sum = 0;
        m_size = size;
    }

    public double next(int val) {
        if(m_curr_size == m_size) {
            m_sum -= m_window.remove(0);
            m_curr_size--;
        }

        m_window.add(val);
        m_sum += val;
        m_curr_size++;
        return (double)m_sum/(double)m_curr_size;
    }
}
```

